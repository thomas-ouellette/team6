# team6

Final project for PHP course (420-524-DW). Web application for tracking one's 
carbon emissions while traveling to/from school or between any 2 locations. 
Created by Thomas Ouellette and Cadin Londono using Laravel and the HERE api.

Heroku URL: https://sustaincadinthomas.herokuapp.com

Test Users: 
cadinsl@gmail.com   password:testtest
ceoofco2@test.com   password:testtest